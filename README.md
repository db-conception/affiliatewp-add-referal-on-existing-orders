# affiliatewp add referal on existing orders

Php script to add referal on existing orders

## Steps to run

1. Add fix-affiliate-order.php to your child theme
1. Create page and set "Fix affiliate order" as template
1. Run this script to get orders ids (replace the coupon id) 

    `SELECT order_id FROM taahir_wc_order_coupon_lookup
left join taahir_affiliate_wp_referrals aff on aff.reference = order_id 
inner join taahir_posts post on post.id = order_id
WHERE coupon_id = 34244 and  aff.reference is null  
and post.post_status in ("wc-processing", "wc-completed")
ORDER BY post.post_date DESC`
1. Change the affiliate id and orders ids in the script 
1. Run the script by loading the page
